package Clases;

public class Sala {

	private String Id_Sala;
	
	private int Piso;
	
	private int Numero;
	
	private Boolean Disponibilidad;
	
	private int Butacas;
	
	private int Butacas_Disponibles;
	
	public Sala(String Id_Sala,int Piso,int Numero,Boolean Disponibilidad,int Butacas,int Butacas_Disponibles) {
		this.Id_Sala=Id_Sala;
		this.Piso=Piso;
		this.Numero=Numero;
		this.Disponibilidad=Disponibilidad;
		this.Butacas=Butacas;
		this.Butacas_Disponibles=Butacas_Disponibles;
	}

	public String getId_Sala() {
		return Id_Sala;
	}

	public int getPiso() {
		return Piso;
	}

	public int getNumero() {
		return Numero;
	}

	public Boolean getDisponibilidad() {
		return Disponibilidad;
	}

	public int getButacas() {
		return Butacas;
	}

	public int getButacas_Disponibles() {
		return Butacas_Disponibles;
	}
	
	
}
