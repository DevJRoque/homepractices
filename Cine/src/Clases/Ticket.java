package Clases;

import java.util.Date;

public class Ticket {

	private String Id_Ticket;
	
	private Date Fecha;
	
	private String Id_Cliente;
	
	private String Pelicula;
	
	private String Sala;
	
	private String Horario;
	
	public Ticket(String Id_Ticket,Date Fecha,String Id_Cliente,String Pelicula,String Sala,String Horario) {
		this.Id_Ticket=Id_Ticket;
		this.Fecha=Fecha;
		this.Id_Cliente=Id_Cliente;
		this.Pelicula=Pelicula;
		this.Sala=Sala;
		this.Horario=Horario; 
		
	}

	public String getId_Ticket() {
		return Id_Ticket;
	}

	public Date getFecha() {
		return Fecha;
	}

	public String getId_Cliente() {
		return Id_Cliente;
	}

	public String getPelicula() {
		return Pelicula;
	}

	public String getSala() {
		return Sala;
	}

	public String getHorario() {
		return Horario;
	}
	
	
}
