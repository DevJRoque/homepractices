package Clases;



import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;


import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowStateListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


import javax.swing.*;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Graphics;




public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		frameLogin Login=new frameLogin();
		Login.setVisible(true);
		Login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}
}

class frameLogin extends JFrame implements WindowFocusListener{
	
	public frameLogin() {
		setTitle("CINEX");
		Toolkit tipoSistema=Toolkit.getDefaultToolkit();
		Dimension dimensiones=tipoSistema.getScreenSize();
		int altura=dimensiones.height;
		int anchura=dimensiones.width;
		setLocation(anchura/3,altura/4);
		setSize(400,350);		
		this.setResizable(false);
		add(new laminaPanelPrincipal_1());
		setBackground(Color.blue);
		addWindowFocusListener(this);
		
	}

	@Override
	public void windowGainedFocus(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowLostFocus(WindowEvent e) {
	
		this.setVisible(false);
	}

}


class frameBoleteria extends JFrame{
	
	public frameBoleteria() {
		setTitle("Boleteria");
		setBounds(200, 400,200	, 500);
		setBackground(Color.red);
		setLocationRelativeTo(null);
	}
}

class laminaPanelPrincipal_1 extends JPanel  {
	
	

	JLabel titulo=new JLabel("CINEX");
	JLabel usuario=new JLabel("Usuario:");
	JTextField cajaUsuario=new JTextField();
	JLabel contraseña=new JLabel("Contraseña:");
	JPasswordField cajaContraseña=new JPasswordField();		
	validacion btnIngresar=new validacion();
	labelLink nombrelink=new labelLink("¿Olvidó su contraseña?","www.gmail.com");
		
	
	
	public laminaPanelPrincipal_1() {
		
		setLayout(null);
		setBackground(Color.darkGray.brighter().brighter());
		//TITULO
		titulo.setFont(new Font("ARIAL",Font.BOLD,16));
		titulo.setBounds(170, 20, 70, 17);
		titulo.setForeground(new Color(146, 124, 101).darker().darker().darker());
		//Texto Usuario
		usuario.setBounds(70, 60, 50, 25);
		usuario.setForeground(new Color(49, 64, 119).darker());
		usuario.setFont(new Font("Arial",Font.BOLD,12));
		//Caja Usuario
		cajaUsuario.setBounds(70,90,150,20);
		//Texto Contraseña
		contraseña.setBounds(70, 120, 80, 25);
		contraseña.setForeground(new Color(49, 64, 119).darker());
		contraseña.setFont(new Font("Arial",Font.BOLD,12));
		//Caja Contraseña
		cajaContraseña.setBounds(70,150,150,20);

	
		
		add(titulo);
		add(usuario);
		add(cajaUsuario);
		add(contraseña);
		add(cajaContraseña);
		add(btnIngresar);
		add(nombrelink);
		
	
	}

		// Boton login
			
			private class validacion extends JButton implements ActionListener{
				
				public validacion() {
						
					this.usuario=usuario;
					setBounds(150,205,100,28);
					setForeground(Color.black.brighter().darker());
					setBackground(Color.white);
					setBorderPainted(false);
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					setText("INGRESAR");
					addActionListener(this);
					
					
				}
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if (cajaUsuario.getText().equalsIgnoreCase("JOSHUA")) {
						JOptionPane.showMessageDialog(null, "Bienvendio "+cajaUsuario.getText());
						new frameBoleteria().setVisible(true);
						new frameLogin();
					}else {
						JOptionPane.showMessageDialog(null,"Usuario no encontrado");
					}
					
				}
				
				private String usuario;
				
				
			}
			
	//Link Recuperacion
			
			
				public class labelLink extends JLabel{
	
				
					private String url,texto;
					
					public labelLink(String texto,String url) {
						
						this.texto=texto;
						this.url=url;
						setBounds(70,245, 134, 22);
						setForeground(Color.red.darker());
						setText(texto);
						addMouseListener(new Link(url));
					
					}
					public class Link extends MouseAdapter{

						private String url;
						
						public Link(String url) {
							this.url=url;
							
						}
						
						public void mouseClicked(MouseEvent e) {
							
							try {
								Desktop.getDesktop().browse(new URI(url));
							} catch (IOException ex) {
								ex.getMessage();
							} catch (URISyntaxException ex) {
								ex.printStackTrace();
							}
						}
						public void mouseEntered(MouseEvent e) {
							setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						}
						public void mouseExited(MouseEvent e) {
							setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						}
					}
	
				
				}
							
			//Linea
			
			public void paintComponent(Graphics g) {
				
				super.paintComponent(g);
				Graphics2D g2=(Graphics2D)g;
				g2.drawLine(300, 243, 70, 243);
			}

			
			
}



