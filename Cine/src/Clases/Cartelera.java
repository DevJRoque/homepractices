package Clases;

public class Cartelera {


	private String Id_Cartelera;
	
	private String Id_Pelicula;
	
	private String Id_Sala;
	
	private int Hora_Inicio;
	
	private int Hora_Final;
	
	public Cartelera(String Id_Cartelera,String Id_Pelicula,String Id_Sala,int Hora_Inicio,int Hora_Final) {
		this.Id_Cartelera=Id_Cartelera;
		this.Id_Pelicula=Id_Pelicula;
		this.Id_Sala=Id_Sala;
		this.Hora_Inicio=Hora_Inicio;
		this.Hora_Final=Hora_Final;
	}

	public String getId_Cartelera() {
		return Id_Cartelera;
	}

	public String getId_Pelicula() {
		return Id_Pelicula;
	}

	public String getId_Sala() {
		return Id_Sala;
	}

	public int getHora_Inicio() {
		return Hora_Inicio;
	}

	public int getHora_Final() {
		return Hora_Final;
	}
	
	
	
	
	
	
	
	
}
