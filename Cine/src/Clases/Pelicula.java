package Clases;

public class Pelicula {

	private String Id_Pelicula;
	
	private String Pelicula;
	
	private String Id_Sala;
	
	private String Genero;
	
	private String Edad_Permitida;
	
	private String Duracion;
	
	private String Formato;
	
	public Pelicula(String Id_Pelicula,String Pelicula,String Id_Sala,String Genero,String Edad_Permitida,String Duracion,String Formato) {
		
		this.Id_Pelicula=Id_Pelicula;
		this.Pelicula=Pelicula;
		this.Id_Sala=Id_Sala;
		this.Genero=Genero;
		this.Edad_Permitida=Edad_Permitida;
		this.Duracion=Duracion;
		this.Formato=Formato;
	}

	public String getId_Pelicula() {
		return Id_Pelicula;
	}

	public String getPelicula() {
		return Pelicula;
	}

	public String getId_Sala() {
		return Id_Sala;
	}

	public String getGenero() {
		return Genero;
	}

	public String getEdad_Permitida() {
		return Edad_Permitida;
	}

	public String getDuracion() {
		return Duracion;
	}

	public String getFormato() {
		return Formato;
	}
	
	
	
	
	
}
